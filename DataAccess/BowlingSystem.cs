﻿using Models;

namespace DataAccess
{
    using Resources.OrderStatus;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;


    public class Bowling_System : DbContext
    {
        public Bowling_System()
            : base("name=Bowling_System")
        {
        }
        
        public virtual DbSet<Order> Orders {get; set;}
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Bowling_club> Clubs { get; set; }
        public virtual DbSet<Lane> Lanes { get; set; }
        public virtual DbSet<Season_Ticket> SeasonTickets { get; set; }
        public virtual DbSet<Trainer> Trainers { get; set; }
        public virtual DbSet<ClubCaffeTable> Tables { get; set; }
    }
}