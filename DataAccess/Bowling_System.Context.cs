﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Bowling_System : DbContext
    {
        public Bowling_System()
            : base("name=Bowling_System")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Bowling_club> Bowling_club { get; set; }
        public virtual DbSet<ClubCaffeTables> ClubCaffeTables { get; set; }
        public virtual DbSet<Lanes> Lanes { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Season_Ticket> Season_Ticket { get; set; }
        public virtual DbSet<Trainers> Trainers { get; set; }
        public virtual DbSet<Users> Users { get; set; }
    }
}
