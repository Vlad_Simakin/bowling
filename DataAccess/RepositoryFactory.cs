﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.RepositoriesInterfaces;
using DataAccess.Repositories;

namespace DataAccess
{
    public static class RepositoryFactory
    {
        public static IOrderRepository MakeOrderRepository(Bowling_System dbContext)
        {
            return new OrderRepository(dbContext);
        }

        public static IUserRepository MakeUsersRepository(Bowling_System dbContext)
        {
            return new UserRepository(dbContext);
        }

        public static ITrainerRepository MakeTrainersRepository(Bowling_System dbContext)
        {
            return new TrainerRepository(dbContext);
        }

        public static IClubReposotory MakeClubsRepository(Bowling_System dbContext)
        {
            return new ClubRepository(dbContext);
        }
    }
}