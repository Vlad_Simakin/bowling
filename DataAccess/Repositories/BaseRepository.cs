﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using System.Data.Entity;

namespace DataAccess.Repositories
{
    public class BaseRepository< T > where T : class
    {
        protected BaseRepository ( Bowling_System db, DbSet< T > dbSet )
        {
            this.db = db;
            this.dbSet     = dbSet;
        }


        protected Bowling_System GetDBContext ()
        {
            return this.db;
        }


        protected DbSet GetDBSet ()
        {
            return this.dbSet;
        }


        public void Add ( T obj )
        {
            dbSet.Add( obj );
        }

        public void Update(T item)
        {
            db.Entry(item).State =EntityState.Modified;
        }

        public void SatatusAdded(T item)
        {
            db.Entry(item).State = EntityState.Unchanged;
        }

        public void Commit ()
        {
            db.ChangeTracker.DetectChanges();
            db.SaveChanges();
        }


        public IQueryable< T > GetAll ()
        {
            return dbSet;
        }


        public T Find ( int id )
        {
            return dbSet.Find( id );
        }

        public void Remove(T item)
        {
            dbSet.Remove(item);
        }


        private Bowling_System db;
        private DbSet< T > dbSet;
    }
}
