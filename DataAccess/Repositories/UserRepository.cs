﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.RepositoriesInterfaces;
//using Models;
using DataAccess;

namespace DataAccess.Repositories
{
    class UserRepository: BaseRepository<Users>,IUserRepository
    {
        public UserRepository(Bowling_System db):
            base(db,db.Users)
        {
        }

        public Users FindByNickname(string nickname)
        {
            return GetDBContext().Users.FirstOrDefault(x => x.Nickname == nickname);
        }

        public Users FindByEmail(string email)
        {
            return GetDBContext().Users.FirstOrDefault(x => x.Email == email);
        }
    }
}
