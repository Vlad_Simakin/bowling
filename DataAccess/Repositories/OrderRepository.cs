﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.RepositoriesInterfaces;

namespace DataAccess.Repositories
{
    class OrderRepository: BaseRepository<Orders>,IOrderRepository
    {
        public OrderRepository(Bowling_System db):
            base(db, db.Orders)
        {

        }
    }
}
