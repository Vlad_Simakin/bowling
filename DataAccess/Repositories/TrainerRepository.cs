﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.RepositoriesInterfaces;
//using Models;
using DataAccess;

namespace DataAccess.Repositories
{
    class TrainerRepository: BaseRepository<Trainers>,ITrainerRepository
    {
        public TrainerRepository(Bowling_System db):
            base(db, db.Trainers)
        {

        }

        public Trainers GetBySecondName(string SecondName)
        {
            return GetDBContext().Trainers.First(x => x.SecondName == SecondName);
        }
    }
}
