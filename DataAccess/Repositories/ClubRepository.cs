﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.RepositoriesInterfaces;

namespace DataAccess.Repositories
{
    class ClubRepository: BaseRepository<Bowling_club>,IClubReposotory
    {
        public ClubRepository(Bowling_System db):
            base(db, db.Bowling_club)
        {

        }
    }
}
