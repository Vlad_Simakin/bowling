﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.RepositoriesInterfaces
{
    public interface IRepository<T> where T : class
    {
        T Find(int id);

        IQueryable<T> GetAll();

        void Add(T t);

        void Update(T item);

        void SatatusAdded(T item);

        void Commit();

        void Remove(T item);
    }
}
