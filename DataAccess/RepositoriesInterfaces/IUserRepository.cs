﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
//using Models;

namespace DataAccess.RepositoriesInterfaces
{
    public interface IUserRepository: IRepository<Users>
    {
        Users FindByNickname(string nickname);
        Users FindByEmail(string email);
    }
}
