
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/03/2015 21:34:45
-- Generated from EDMX file: C:\Users\Vlad\Desktop\Bowling system\DataAccess\Bowling_System.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [New];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_dbo_ClubCaffeTables_dbo_Bowling_club_Club_Id]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ClubCaffeTables] DROP CONSTRAINT [FK_dbo_ClubCaffeTables_dbo_Bowling_club_Club_Id];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_Lanes_dbo_Bowling_club_Club_Id]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Lanes] DROP CONSTRAINT [FK_dbo_Lanes_dbo_Bowling_club_Club_Id];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_Orders_dbo_Bowling_club_Club_Id]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_dbo_Orders_dbo_Bowling_club_Club_Id];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_Orders_dbo_ClubCaffeTables_Table_Id]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_dbo_Orders_dbo_ClubCaffeTables_Table_Id];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_Orders_dbo_Trainers_Trainer_Id]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_dbo_Orders_dbo_Trainers_Trainer_Id];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_Orders_dbo_Users_User_Id]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_dbo_Orders_dbo_Users_User_Id];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_Season_Ticket_dbo_Users_User_Id]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Season_Ticket] DROP CONSTRAINT [FK_dbo_Season_Ticket_dbo_Users_User_Id];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[__MigrationHistory]', 'U') IS NOT NULL
    DROP TABLE [dbo].[__MigrationHistory];
GO
IF OBJECT_ID(N'[dbo].[Bowling_club]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Bowling_club];
GO
IF OBJECT_ID(N'[dbo].[ClubCaffeTables]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClubCaffeTables];
GO
IF OBJECT_ID(N'[dbo].[Lanes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Lanes];
GO
IF OBJECT_ID(N'[dbo].[Orders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Orders];
GO
IF OBJECT_ID(N'[dbo].[Season_Ticket]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Season_Ticket];
GO
IF OBJECT_ID(N'[dbo].[Trainers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Trainers];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'C__MigrationHistory'
CREATE TABLE [dbo].[C__MigrationHistory] (
    [MigrationId] nvarchar(150)  NOT NULL,
    [ContextKey] nvarchar(300)  NOT NULL,
    [Model] varbinary(max)  NOT NULL,
    [ProductVersion] nvarchar(32)  NOT NULL
);
GO

-- Creating table 'Bowling_club'
CREATE TABLE [dbo].[Bowling_club] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ClubName] nvarchar(max)  NULL,
    [Address] nvarchar(max)  NULL,
    [Opening] int  NOT NULL,
    [Closing] int  NOT NULL
);
GO

-- Creating table 'ClubCaffeTables'
CREATE TABLE [dbo].[ClubCaffeTables] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TableNumber] int  NOT NULL,
    [TimeTable] nvarchar(max)  NULL,
    [Club_Id] int  NULL
);
GO

-- Creating table 'Lanes'
CREATE TABLE [dbo].[Lanes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [LaneNumber] int  NOT NULL,
    [HighScore] int  NOT NULL,
    [TimeTable] nvarchar(max)  NULL,
    [Club_Id] int  NULL
);
GO

-- Creating table 'Orders'
CREATE TABLE [dbo].[Orders] (
    [OrderId] int IDENTITY(1,1) NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [OrderedFrom] int  NOT NULL,
    [OrderedFor] int  NOT NULL,
    [Status] int  NOT NULL,
    [Price] decimal(18,2)  NOT NULL,
    [Discount] decimal(18,2)  NOT NULL,
    [LaneNumbers] nvarchar(max)  NULL,
    [Club_Id] int  NULL,
    [Table_Id] int  NULL,
    [Trainer_Id] int  NULL,
    [User_Id] int  NULL
);
GO

-- Creating table 'Season_Ticket'
CREATE TABLE [dbo].[Season_Ticket] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Price] decimal(18,2)  NOT NULL,
    [ForMonths] int  NOT NULL,
    [ExpirationDate] datetime  NOT NULL,
    [User_Id] int  NULL
);
GO

-- Creating table 'Trainers'
CREATE TABLE [dbo].[Trainers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [SecondName] nvarchar(max)  NULL,
    [Email] nvarchar(max)  NULL,
    [Phone] nvarchar(max)  NULL,
    [TimeTable] nvarchar(max)  NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nickname] nvarchar(max)  NULL,
    [UserName] nvarchar(max)  NULL,
    [SecondName] nvarchar(max)  NULL,
    [HighScore] int  NOT NULL,
    [HasCancellations] bit  NOT NULL,
    [Amount] decimal(18,2)  NOT NULL,
    [CreditCard] nvarchar(max)  NULL,
    [Email] nvarchar(50)  NULL,
    [Phone] nvarchar(20)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [MigrationId], [ContextKey] in table 'C__MigrationHistory'
ALTER TABLE [dbo].[C__MigrationHistory]
ADD CONSTRAINT [PK_C__MigrationHistory]
    PRIMARY KEY CLUSTERED ([MigrationId], [ContextKey] ASC);
GO

-- Creating primary key on [Id] in table 'Bowling_club'
ALTER TABLE [dbo].[Bowling_club]
ADD CONSTRAINT [PK_Bowling_club]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClubCaffeTables'
ALTER TABLE [dbo].[ClubCaffeTables]
ADD CONSTRAINT [PK_ClubCaffeTables]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Lanes'
ALTER TABLE [dbo].[Lanes]
ADD CONSTRAINT [PK_Lanes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [OrderId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [PK_Orders]
    PRIMARY KEY CLUSTERED ([OrderId] ASC);
GO

-- Creating primary key on [Id] in table 'Season_Ticket'
ALTER TABLE [dbo].[Season_Ticket]
ADD CONSTRAINT [PK_Season_Ticket]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Trainers'
ALTER TABLE [dbo].[Trainers]
ADD CONSTRAINT [PK_Trainers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Club_Id] in table 'ClubCaffeTables'
ALTER TABLE [dbo].[ClubCaffeTables]
ADD CONSTRAINT [FK_dbo_ClubCaffeTables_dbo_Bowling_club_Club_Id]
    FOREIGN KEY ([Club_Id])
    REFERENCES [dbo].[Bowling_club]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_ClubCaffeTables_dbo_Bowling_club_Club_Id'
CREATE INDEX [IX_FK_dbo_ClubCaffeTables_dbo_Bowling_club_Club_Id]
ON [dbo].[ClubCaffeTables]
    ([Club_Id]);
GO

-- Creating foreign key on [Club_Id] in table 'Lanes'
ALTER TABLE [dbo].[Lanes]
ADD CONSTRAINT [FK_dbo_Lanes_dbo_Bowling_club_Club_Id]
    FOREIGN KEY ([Club_Id])
    REFERENCES [dbo].[Bowling_club]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_Lanes_dbo_Bowling_club_Club_Id'
CREATE INDEX [IX_FK_dbo_Lanes_dbo_Bowling_club_Club_Id]
ON [dbo].[Lanes]
    ([Club_Id]);
GO

-- Creating foreign key on [Club_Id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_dbo_Orders_dbo_Bowling_club_Club_Id]
    FOREIGN KEY ([Club_Id])
    REFERENCES [dbo].[Bowling_club]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_Orders_dbo_Bowling_club_Club_Id'
CREATE INDEX [IX_FK_dbo_Orders_dbo_Bowling_club_Club_Id]
ON [dbo].[Orders]
    ([Club_Id]);
GO

-- Creating foreign key on [Table_Id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_dbo_Orders_dbo_ClubCaffeTables_Table_Id]
    FOREIGN KEY ([Table_Id])
    REFERENCES [dbo].[ClubCaffeTables]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_Orders_dbo_ClubCaffeTables_Table_Id'
CREATE INDEX [IX_FK_dbo_Orders_dbo_ClubCaffeTables_Table_Id]
ON [dbo].[Orders]
    ([Table_Id]);
GO

-- Creating foreign key on [Trainer_Id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_dbo_Orders_dbo_Trainers_Trainer_Id]
    FOREIGN KEY ([Trainer_Id])
    REFERENCES [dbo].[Trainers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_Orders_dbo_Trainers_Trainer_Id'
CREATE INDEX [IX_FK_dbo_Orders_dbo_Trainers_Trainer_Id]
ON [dbo].[Orders]
    ([Trainer_Id]);
GO

-- Creating foreign key on [User_Id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_dbo_Orders_dbo_Users_User_Id]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_Orders_dbo_Users_User_Id'
CREATE INDEX [IX_FK_dbo_Orders_dbo_Users_User_Id]
ON [dbo].[Orders]
    ([User_Id]);
GO

-- Creating foreign key on [User_Id] in table 'Season_Ticket'
ALTER TABLE [dbo].[Season_Ticket]
ADD CONSTRAINT [FK_dbo_Season_Ticket_dbo_Users_User_Id]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_Season_Ticket_dbo_Users_User_Id'
CREATE INDEX [IX_FK_dbo_Season_Ticket_dbo_Users_User_Id]
ON [dbo].[Season_Ticket]
    ([User_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------