//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Orders
    {
        public int OrderId { get; set; }
        public System.DateTime OrderDate { get; set; }
        public int OrderedFrom { get; set; }
        public int OrderedFor { get; set; }
        public int Status { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public string LaneNumbers { get; set; }
        public Nullable<int> Club_Id { get; set; }
        public Nullable<int> Table_Id { get; set; }
        public Nullable<int> Trainer_Id { get; set; }
        public Nullable<int> User_Id { get; set; }
    
        public virtual Bowling_club Bowling_club { get; set; }
        public virtual ClubCaffeTables ClubCaffeTables { get; set; }
        public virtual Trainers Trainers { get; set; }
        public virtual Users Users { get; set; }
    }
}
