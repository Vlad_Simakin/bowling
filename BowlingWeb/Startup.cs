﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BowlingWeb.Startup))]
namespace BowlingWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
