﻿CREATE TABLE [dbo].[Bowling_club] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [ClubName] NVARCHAR (MAX) NULL,
    [Address]  NVARCHAR (MAX) NULL,
    [Opening]  INT            CONSTRAINT [DF_Bowling_club_Opening] DEFAULT ((12)) NOT NULL,
    [Closing]  INT            CONSTRAINT [DF_Bowling_club_Closing] DEFAULT ((2)) NOT NULL,
    CONSTRAINT [PK_dbo.Bowling_club] PRIMARY KEY CLUSTERED ([Id] ASC)
);



