﻿CREATE TABLE [dbo].[Orders] (
    [OrderId]     INT             IDENTITY (1, 1) NOT NULL,
    [OrderDate]   DATETIME        NOT NULL,
    [OrderedFrom] INT             NOT NULL,
    [OrderedFor]  INT             NOT NULL,
    [Status]      INT             NOT NULL,
    [Price]       DECIMAL (18, 2) NOT NULL,
    [Discount]    DECIMAL (18, 2) NOT NULL,
    [LaneNumbers] NVARCHAR (MAX)  NULL,
    [Club_Id]     INT             NULL,
    [Table_Id]    INT             NULL,
    [Trainer_Id]  INT             NULL,
    [User_Id]     INT             NULL,
    CONSTRAINT [PK_dbo.Orders] PRIMARY KEY CLUSTERED ([OrderId] ASC),
    CONSTRAINT [FK_dbo.Orders_dbo.Bowling_club_Club_Id] FOREIGN KEY ([Club_Id]) REFERENCES [dbo].[Bowling_club] ([Id]),
    CONSTRAINT [FK_dbo.Orders_dbo.ClubCaffeTables_Table_Id] FOREIGN KEY ([Table_Id]) REFERENCES [dbo].[ClubCaffeTables] ([Id]),
    CONSTRAINT [FK_dbo.Orders_dbo.Trainers_Trainer_Id] FOREIGN KEY ([Trainer_Id]) REFERENCES [dbo].[Trainers] ([Id]),
    CONSTRAINT [FK_dbo.Orders_dbo.Users_User_Id] FOREIGN KEY ([User_Id]) REFERENCES [dbo].[Users] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Club_Id]
    ON [dbo].[Orders]([Club_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Table_Id]
    ON [dbo].[Orders]([Table_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Trainer_Id]
    ON [dbo].[Orders]([Trainer_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_User_Id]
    ON [dbo].[Orders]([User_Id] ASC);

