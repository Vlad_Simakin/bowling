﻿CREATE TABLE [dbo].[Trainers] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (MAX) NULL,
    [SecondName] NVARCHAR (MAX) NULL,
    [Email]      NVARCHAR (MAX) NULL,
    [Phone]      NVARCHAR (MAX) NULL,
    [TimeTable]  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.Trainers] PRIMARY KEY CLUSTERED ([Id] ASC)
);



