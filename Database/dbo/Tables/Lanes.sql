﻿CREATE TABLE [dbo].[Lanes] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [LaneNumber] INT            NOT NULL,
    [HighScore]  INT            NOT NULL,
    [TimeTable]  NVARCHAR (MAX) NULL,
    [Club_Id]    INT            NULL,
    CONSTRAINT [PK_dbo.Lanes] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Lanes_dbo.Bowling_club_Club_Id] FOREIGN KEY ([Club_Id]) REFERENCES [dbo].[Bowling_club] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Club_Id]
    ON [dbo].[Lanes]([Club_Id] ASC);

