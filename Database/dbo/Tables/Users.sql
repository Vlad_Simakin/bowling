﻿CREATE TABLE [dbo].[Users] (
    [Id]               INT             IDENTITY (1, 1) NOT NULL,
    [Nickname]         NVARCHAR (MAX)  NULL,
    [UserName]         NVARCHAR (MAX)  NULL,
    [SecondName]       NVARCHAR (MAX)  NULL,
    [HighScore]        INT             NOT NULL,
    [HasCancellations] BIT             NOT NULL,
    [Amount]           DECIMAL (18, 2) NOT NULL,
    [CreditCard]       NVARCHAR (MAX)  NULL,
    [Email]            NVARCHAR (50)   NULL,
    [Phone]            NVARCHAR (20)   NULL,
    CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED ([Id] ASC)
);



