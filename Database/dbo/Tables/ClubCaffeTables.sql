﻿CREATE TABLE [dbo].[ClubCaffeTables] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [TableNumber] INT            NOT NULL,
    [TimeTable]   NVARCHAR (MAX) NULL,
    [Club_Id]     INT            NULL,
    CONSTRAINT [PK_dbo.ClubCaffeTables] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ClubCaffeTables_dbo.Bowling_club_Club_Id] FOREIGN KEY ([Club_Id]) REFERENCES [dbo].[Bowling_club] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Club_Id]
    ON [dbo].[ClubCaffeTables]([Club_Id] ASC);

