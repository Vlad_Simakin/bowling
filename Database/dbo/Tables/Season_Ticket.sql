﻿CREATE TABLE [dbo].[Season_Ticket] (
    [Id]             INT             IDENTITY (1, 1) NOT NULL,
    [Price]          DECIMAL (18, 2) NOT NULL,
    [ForMonths]      INT             NOT NULL,
    [ExpirationDate] DATETIME        NOT NULL,
    [User_Id]        INT             NULL,
    CONSTRAINT [PK_dbo.Season_Ticket] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Season_Ticket_dbo.Users_User_Id] FOREIGN KEY ([User_Id]) REFERENCES [dbo].[Users] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_User_Id]
    ON [dbo].[Season_Ticket]([User_Id] ASC);

