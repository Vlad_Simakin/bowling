﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Controllers.ViewModels
{
    public class TableViewModel:BasicViewModel<TableViewModel>
    {
        public int TableNumber { get; set; }
        public Graph TimeTable { get; set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>() {TableNumber, TimeTable};
        }

        public override string ToString()
        {
            StringBuilder res = new StringBuilder();
            res.AppendFormat("Table Number: {0}\n", TableNumber);
            res.Append(TimeTable.ToString());
            return res.ToString();
        }
    }
}
