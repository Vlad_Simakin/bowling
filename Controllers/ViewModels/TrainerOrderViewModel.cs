﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers.ViewModels
{
    public class TrainerOrderViewModel:BasicViewModel<TrainerViewModel>
    {
        public DateTime OrderDate { get; set; }
        public int OrderFrom { get; set; }
        public int OrderFor { get; set; }
        public string LaneNumbers { get; set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>() {OrderDate, OrderFrom, OrderFor, LaneNumbers};
        }

        public override string ToString()
        {
            return String.Format("Date: {0}\n From: {1}\nFor: {2}\nLanes: {3}", OrderDate, OrderFrom, OrderFor, LaneNumbers);
        }
    }
}
