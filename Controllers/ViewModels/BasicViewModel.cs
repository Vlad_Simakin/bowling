﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Controllers.ViewModels
{
    public abstract class BasicViewModel<TConcreteView>
        where TConcreteView : BasicViewModel<TConcreteView>
    {
        public override bool Equals(object other)
        {
            return Equals(other as TConcreteView);
        }

        public bool Equals(TConcreteView other)
        {
            if (other == null)
                return false;

            var sequence1 = GetAttributesToIncludeInEqualityCheck();
            var sequence2 = other.GetAttributesToIncludeInEqualityCheck();
            return sequence1.SequenceEqual(sequence2);
        }

        public static bool operator ==(BasicViewModel<TConcreteView> left,
                                         BasicViewModel<TConcreteView> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(BasicViewModel<TConcreteView> left,
                                         BasicViewModel<TConcreteView> right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            int hash = 17;
            foreach (var obj in this.GetAttributesToIncludeInEqualityCheck())
                hash = hash * 31 + (obj == null ? 0 : obj.GetHashCode());
            return hash;
        }

        protected abstract IEnumerable<object> GetAttributesToIncludeInEqualityCheck();
    }
}
