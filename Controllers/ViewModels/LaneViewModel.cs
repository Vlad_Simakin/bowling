﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Controllers.ViewModels
{
    public class LaneViewModel:BasicViewModel<LaneViewModel>
    {
        public int LaneNumber { get; set;}
        public int HighScore { get; set; }
        public Graph TimeTable { get; set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>() { LaneNumber, HighScore, TimeTable };
        }
        public override string ToString()
        {
            StringBuilder res = new StringBuilder();
            res.AppendFormat("Lane Number: {0}\nHighScore: {1}\n", LaneNumber, HighScore);
            res.Append(TimeTable.ToString());
            return res.ToString();
        }
    }
}
