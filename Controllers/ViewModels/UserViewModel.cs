﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers.ViewModels
{
    public class UserViewModel: BasicViewModel<UserViewModel>
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
        public string UserName { get; set; }
        public string SecondName { get; set; }
        public decimal Amount { get; set; }
        public int HighScore { get; set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>() {Id,Nickname, UserName, SecondName, Amount, HighScore};
        }

        public override string ToString()
        {
            return string.Format("Id: {5}\nNickname: {0}\nFirst Name: {1}\nSecond Name: {2}\nAmount: {3}\nHighScore: {4}\n",
                Nickname,
                UserName,
                SecondName,
                Amount,
                HighScore,
                Id);
        }
    }
}
