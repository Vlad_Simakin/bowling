﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Controllers.ViewModels
{
    public class OrderViewModel: BasicViewModel<OrderViewModel>
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public int OrderedFrom { get; set; }
        public int OrderedFor { get; set; }
        public int Status { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public string LaneNumbers { get; set; }
        public string ClubName { get; set; }
        public string UserNickname { get; set; }
        public string TrainerFullName { get; set; }
        public int Table { get; set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>()
            {
                OrderId,
                OrderDate,
                OrderedFrom,
                OrderedFor,
                LaneNumbers,
                Price,
                Discount,
                Status,
                ClubName,
                UserNickname,
                TrainerFullName,
                Table
            };
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.AppendFormat(
                "Id: {10}\nOrder Date: {0}\nOrdered From: {1}\nOrdered For: {2}\nStatus: {3}\nPrice: {4}%\nDiscount: {5}\nLane Numbers: {6}\nTable: {7}\nUser Name: {8}\nClub Name: {9}\n",
                OrderDate,
                OrderedFrom,
                OrderedFor,
                Status,
                Price,
                Discount,
                LaneNumbers,
                Table,
                UserNickname,
                ClubName,
                OrderId);
            if (!String.IsNullOrEmpty(TrainerFullName))
            {
                result.AppendFormat("Trainer: {0}", TrainerFullName);
            }

            return result.ToString();
        }
    }
}
