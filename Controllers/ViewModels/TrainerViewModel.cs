﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers.ViewModels
{
    public class TrainerViewModel:BasicViewModel<TrainerViewModel>
    {
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>() {Name, SecondName, Email, Phone};
        }

        public override string ToString()
        {
            return string.Format("Name: {0}\nSurname: {1}\nEmail: {2}\nPhone: {3}",
                Name,
                SecondName,
                Email,
                Phone);
        }
    }
}
