﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace Controllers.ViewModels
{
    public class BowlingViewModel:BasicViewModel<BowlingViewModel>
    {
        public int Id { get; set; }
        public string ClubName { get; set; }
        public string Address { get; set; }
        public int Opening { get; set; }
        public int Closing { get; set; }
        public int Lanes { get; set; }
        public int Tables { get; set; }


        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object> { Id,ClubName, Address, Opening, Closing,Lanes, Tables };
        }


        public override string ToString()
        {
            return string.Format(
                       "Id = {6}\nName = {0}\nAddress = {1}\nOpening = {2}\nClosing = {3}\nLanes = {4}\nTables = {5}\n",
                       ClubName,
                       Address,
                       Opening,
                       Closing,
                       Lanes,
                       Tables,
                       Id
                   );
        }
    }
}
