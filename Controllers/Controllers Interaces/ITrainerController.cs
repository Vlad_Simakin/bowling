﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Models;
using Controllers.ViewModels;
using DataAccess;

namespace Controllers.Controllers_Interaces
{
    public interface ITrainerController : IDisposable
    {
        int AddTrainer(string Name, string SecondName, string Phone, string Email);
        void RemoverTrainer(int TrainerId);
        TrainerViewModel FindTrainerById(int Id);
        TrainerViewModel FindTrainerBySecondName(string SecondName);
        ICollection<TrainerViewModel> ListOfTrainers();
        ICollection<TrainerOrderViewModel> TrainerOrders(int Id);
    }
}
