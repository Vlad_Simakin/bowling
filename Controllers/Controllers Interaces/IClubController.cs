﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controllers.ViewModels;

namespace Controllers.Controllers_Interaces
{
    public interface IClubController : IDisposable
    {
        int CreateClub(string Name, string Address, int Opening, int Closing,  int Lanes, int CaffeTables = 0);
         BowlingViewModel GetClub(int Id);
         ICollection<BowlingViewModel> GetAllClubs();
        ICollection<LaneViewModel> ClubLanes(int Id);
        ICollection<TableViewModel> ClubTables(int Id);
    }
}
