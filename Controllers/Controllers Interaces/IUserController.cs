﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Models;
using Controllers.ViewModels;
using DataAccess;

namespace Controllers.Controllers_Interaces
{
    public interface IUserController:IDisposable
    {
         int CreateUser(string Nickname, string Name, string SecondName, string Email, string Phone);
         UserViewModel GetUser(int Id);
         UserViewModel GetUserByNickname(string nickname);
         UserViewModel GetUserByEmail(string email);
         ICollection<UserViewModel> GetAllUsers();
        ICollection<OrderViewModel> GetUserHistory(int userId); 
         void BuySeasonTicket(int userId, int months);
    }
}
