﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Models;
using Controllers.ViewModels;
using DataAccess;

namespace Controllers.Controllers_Interaces
{
    public interface IOrderController : IDisposable
    {
        int MakeOrder(int ClubId, int UserId, DateTime OrderDate, int OrderedFrom, int OrderedFor, int LanesOrdered = 1, bool CaffeRequired = false, bool TrainerRequired = false);
        void CancelOrder(int Id);
        OrderViewModel GetOrder(int Id);
        void OrderPaymentPlan(int Id, string cardCode, string cardHolder);
        ICollection<OrderViewModel> GetAllOrders();
        //payment later
    }
}
