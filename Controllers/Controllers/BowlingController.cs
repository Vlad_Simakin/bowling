﻿using System;
using System.Collections;
using System.Linq;
using AutoMapper;
using DataAccess;
using Controllers.Controllers_Interaces;
using DataAccess.RepositoriesInterfaces;
using System.Collections.ObjectModel;
using Controllers.ViewModels;
using System.Collections.Generic;
using Models;

namespace Controllers
{
    class BowlingController :BaseController, IClubController
    {
        private IClubReposotory ClubRepository;
        public BowlingController()
        {
            this.ClubRepository = RepositoryFactory.MakeClubsRepository(GetDBContext());
        }

        public virtual int CreateClub(string Name, string Address, int Opening, int Closing, int Lanes, int CaffeTables = 0)
        {
            if (Lanes == 0)
                throw (new Exception("Incorrect numer of lanes"));
            if (Opening > 23 || Closing > 23)
                throw (new Exception("Incorrect working hours"));

            var newClub = new Bowling_club();

            newClub.Address = Address;
            newClub.ClubName = Name;
            newClub.Opening = (int)Opening;
            newClub.Closing = (int)Closing;
            for (int i = 0; i < Lanes; i++)
            {
                var lane = new Lanes();
                lane.Bowling_club = newClub;
                lane.LaneNumber = i + 1;
                newClub.Lanes.Add(lane);
            }
            for (int i = 0; i < CaffeTables; i++)
            {
                var table = new ClubCaffeTables();
                table.Bowling_club = newClub;
                table.TableNumber = i + 1;
                newClub.ClubCaffeTables.Add(table);
            }

            ClubRepository.Add(newClub);
            ClubRepository.Commit();

            return newClub.Id;
        }


        public virtual BowlingViewModel GetClub(int Id)
        {
            Mapper.CreateMap<Bowling_club, BowlingViewModel>()
                .ForMember("Lanes", opt => opt.MapFrom(x => x.Lanes.Count))
                .ForMember("Tables", opt => opt.MapFrom(x => x.ClubCaffeTables.Count));
            return Mapper.Map<BowlingViewModel>(ClubRepository.Find(Id));
        }

        public ICollection<BowlingViewModel> GetAllClubs()
        {
            Mapper.CreateMap<Bowling_club, BowlingViewModel>()
                .ForMember("Lanes", opt => opt.MapFrom(x => x.Lanes.Count))
                .ForMember("Tables", opt => opt.MapFrom(x => x.ClubCaffeTables.Count));
            return Mapper.Map<ICollection<BowlingViewModel>>(ClubRepository.GetAll());
        }

        public ICollection<LaneViewModel> ClubLanes(int Id)
        {
            Mapper.CreateMap<Lanes, LaneViewModel>()
                .ForMember("TimeTable", opt => opt.MapFrom(x => Graph.ToObj(x.TimeTable)));
            return Mapper.Map<ICollection<LaneViewModel>>(ClubRepository.Find(Id).Lanes);
        }

        public ICollection<TableViewModel> ClubTables(int Id)
        {
            Mapper.CreateMap<ClubCaffeTables, TableViewModel>()
                .ForMember("TimeTable", opt => opt.MapFrom(x => Graph.ToObj(x.TimeTable)));
            return Mapper.Map<ICollection<TableViewModel>>(ClubRepository.Find(Id).ClubCaffeTables);
        }
    }
}
