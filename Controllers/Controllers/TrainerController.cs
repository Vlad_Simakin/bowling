﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Controllers.Controllers_Interaces;
using Controllers.ViewModels;
using DataAccess.RepositoriesInterfaces;
using DataAccess;
//using Models;

namespace Controllers
{
    class TrainerController : BaseController, ITrainerController
    {
        private ITrainerRepository Repository;

        public TrainerController()
        {
            this.Repository = RepositoryFactory.MakeTrainersRepository(GetDBContext());
        }

        public int AddTrainer(string Name, string SecondName, string Phone, string Email)
        {
            var newTrainer = new Trainers();
            newTrainer.Name = Name;
            newTrainer.SecondName = SecondName;
            newTrainer.Phone = Phone;
            newTrainer.Email = Email;

            Repository.Add(newTrainer);
            Repository.Commit();

            return newTrainer.Id;
        }

        public void RemoverTrainer(int TrainerId)
        {
            var trainer = Repository.Find(TrainerId);
            if (trainer != null)
            {
                Repository.Remove(trainer);
                Repository.Commit();
            }
        }

        public TrainerViewModel FindTrainerById(int Id)
        {
            Mapper.CreateMap<Trainers, TrainerViewModel>();
            return Mapper.Map<TrainerViewModel>(Repository.Find(Id));
        }

        public TrainerViewModel FindTrainerBySecondName(string SecondName)
        {
            Mapper.CreateMap<Trainers, TrainerViewModel>();
            return Mapper.Map<TrainerViewModel>(Repository.GetBySecondName(SecondName));
        }

        public ICollection<TrainerViewModel> ListOfTrainers()
        {
            Mapper.CreateMap<Trainers, TrainerViewModel>();
            return Mapper.Map<ICollection<TrainerViewModel>>(Repository.GetAll());
        }

        public ICollection<TrainerOrderViewModel> TrainerOrders(int Id)
        {
            Mapper.CreateMap<Orders, TrainerOrderViewModel>();
            return Mapper.Map<ICollection<TrainerOrderViewModel>>(Repository.Find(Id).Orders);
        }
    }
}
