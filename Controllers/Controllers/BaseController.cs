﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers
{
    public class BaseController
    {
        protected Bowling_System GetDBContext()
        {
            if (dbContext == null)
                dbContext = new Bowling_System();

            return dbContext;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing && dbContext != null)
                dbContext.Dispose();
        }

        private Bowling_System dbContext;
    }
}
