﻿using System;
using System.Collections.Generic;
using Controllers.Controllers_Interaces;
using DataAccess;
using System.Data.Entity;
using Infrastructure;
using Resources;
using DataAccess.RepositoriesInterfaces;
using System.Collections.ObjectModel;
using Controllers.ViewModels;
using AutoMapper;

namespace Controllers
{
    class UserController :BaseController, IUserController
    {
        private readonly IUserRepository UserRepository;

        public UserController()
        {
            this.UserRepository = RepositoryFactory.MakeUsersRepository(GetDBContext());
        }

        public virtual int CreateUser(string Nickname, string Name, string SecondName, string Email, string Phone)
        {
            var newUser = new Users();
            newUser.Nickname = Nickname;
            newUser.HasCancellations = false;
            newUser.UserName = Name;
            newUser.SecondName = SecondName;
            newUser.Email = Email;
            newUser.Phone = Phone;

            UserRepository.Add(newUser);
            UserRepository.Commit();

            return newUser.Id;
        }

        public virtual UserViewModel GetUserByNickname(string nickname)
        {
            Mapper.CreateMap<Users, UserViewModel>();
            return Mapper.Map<UserViewModel>(UserRepository.FindByNickname(nickname));
        }

        public virtual UserViewModel GetUserByEmail(string email)
        {
            Mapper.CreateMap<Users, UserViewModel>();
            return Mapper.Map<UserViewModel>(UserRepository.FindByEmail(email));
        }

        public virtual UserViewModel GetUser(int Id)
        {
            Mapper.CreateMap<Users, UserViewModel>();
            return Mapper.Map<UserViewModel>(UserRepository.Find(Id));
        }

        public virtual ICollection<UserViewModel> GetAllUsers()
        {
            Mapper.CreateMap<Users, UserViewModel>();
            return Mapper.Map<ICollection<UserViewModel>>(UserRepository.GetAll());
        }

        public ICollection<OrderViewModel> GetUserHistory(int userId)
        {
            Mapper.CreateMap<ICollection<Orders>, ICollection<OrderViewModel>>();
            return Mapper.Map<ICollection<OrderViewModel>>(UserRepository.Find(userId).Orders);
        }

        public void BuySeasonTicket(int userId, int months)
        {
            var user = UserRepository.Find(userId);
            if (user == null)
                throw (new Exception("User with this Id does not exist"));

            var Payment = InfrastructureFactory.MakePaymentAgent();

            int PaymentId;
            if (Payment.makePaymentTransaction(months * Constants.SeasonMonthPrice,
               user.CreditCard, user.SecondName + " " + user.SecondName, out PaymentId))
            {
                var newTicket = new Season_Ticket();
                newTicket.ExpirationDate = DateTime.Now.AddMonths(months);
                newTicket.ForMonths = months;
                newTicket.Price = months * Constants.SeasonMonthPrice;
                user.Amount += newTicket.Price;
                user.Season_Ticket.Add(newTicket);
                UserRepository.Commit();
            }
            else
            {
                throw (new Exception("Something went wrong"));
            }
        }

        //private void DeleteExpired()
        //{
        //    foreach(var ticket in db.SeasonTickets)
        //        if (ticket.ExpirationDate < DateTime.Now)
        //        {
        //            db.SeasonTickets.Remove(ticket);
        //        }
        //    db.SaveChanges();
        //}
    }
}
