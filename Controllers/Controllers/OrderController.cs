﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Controllers.Controllers_Interaces;
using Resources.OrderStatus;
using DataAccess;
using System.Data.Entity;
using System.Collections.ObjectModel;
using DataAccess.RepositoriesInterfaces;
using AutoMapper;
using Controllers.ViewModels;

namespace Controllers
{
    class OrderController : BaseController, IOrderController
    {
        private IOrderRepository OrderRepository;
        private IUserRepository UserRepository;
        private IClubReposotory ClubRepository;
        public OrderController()
        {
            this.ClubRepository = RepositoryFactory.MakeClubsRepository(GetDBContext());
            this.UserRepository = RepositoryFactory.MakeUsersRepository(GetDBContext());
            this.OrderRepository = RepositoryFactory.MakeOrderRepository(GetDBContext());
        }

        public virtual int MakeOrder(int ClubId, int UserId, DateTime OrderDate, int OrderedFrom, int OrderedFor, int LanesOrdered = 1, bool CaffeRequired = false, bool TrainerRequired = false)
        {
            var newOrder = new Orders();
            var Club = GetDBContext().Bowling_club.Find(ClubId);
            var User = GetDBContext().Users.Find(UserId);

            

            if (LanesOrdered > Club.Lanes.Count)
                throw (new Exception("Too many lanes were choosen"));
            if (OrderedFrom < Club.Opening && OrderedFrom > Club.Closing)
                throw (new Exception("Club is closed at this time"));
            if ((OrderedFrom + OrderedFor > Club.Opening && OrderedFrom + OrderedFor < Club.Closing))
                throw (new Exception("You can't order for so long for this time"));

            newOrder.Status = (int)OrderStatus.Created;
            newOrder.OrderDate = OrderDate;
            newOrder.OrderedFrom = OrderedFrom;
            newOrder.OrderedFor = OrderedFor;
            newOrder.LaneNumbers = FindFreeLanes(Club, OrderDate, OrderedFor, OrderedFrom, LanesOrdered);
            if (CaffeRequired)
                newOrder.ClubCaffeTables = FindFreeTable(Club, OrderDate, OrderedFrom, OrderedFor);
            else newOrder.Trainers = null;
            if (TrainerRequired)
            {
                newOrder.Trainers = FindTrainer(OrderDate, OrderedFrom, OrderedFor);
                var smsAgent = Infrastructure.InfrastructureFactory.MakeSmsAgent();
                StringBuilder body = new StringBuilder();
                body.AppendFormat("You have new Order! Your order is lane {0} for {1} at club {2}",
                                    newOrder.LaneNumbers, newOrder.OrderDate.AddHours(newOrder.OrderedFrom), Club.ClubName);
                smsAgent.sendSMS(newOrder.Trainers.Phone, body.ToString());
            }
            else newOrder.Trainers = null;

            if (User != null)
            {
                if (User.Season_Ticket.Count != 0 && User.Season_Ticket.Last().ExpirationDate > DateTime.Now)
                {
                    newOrder.Price = 0m;
                    newOrder.Discount = 0m;
                }
                else
                {
                    newOrder.Price = CountPrice(OrderDate.AddHours(OrderedFrom), OrderedFor, LanesOrdered);
                    newOrder.Discount = CountDiscount(User, newOrder);
                }
                newOrder.Users = User;
                User.Amount += newOrder.Price;
                UserRepository.Update(User);
            }
            else
            {
                //need some personal data then
                newOrder.Price = CountPrice(OrderDate.AddHours(OrderedFrom), OrderedFor, LanesOrdered);
            }
            newOrder.Bowling_club = Club;
            OrderRepository.Add(newOrder);
            ClubRepository.Commit();
            return newOrder.OrderId;
        }

        public void CancelOrder(int Id)
        {
            var order = OrderRepository.Find(Id);

            order.Users.HasCancellations = true;
            order.Status = (int)OrderStatus.Cancelled;

            if ((order.OrderDate - DateTime.Now).Days == 0 && DateTime.Now.Hour - order.OrderedFrom < 3)
            {
                // Payment.ReturnMoney(0.5m);
            }
            else
            {
                //  Payment.ReturnMoney(1m);
            }
            string[] Parts = order.LaneNumbers.Split(',');
            var numbers = new Collection<int>();

            foreach (var part in Parts)
            {
                numbers.Add(int.Parse(part));
            }
            foreach (int number in numbers)
                order.Bowling_club.Lanes.First(x => x.LaneNumber == number).TimeTable =
                    Models.Graph.RemoveReservation(order.Bowling_club.Lanes.First(x => x.LaneNumber == number).TimeTable, order.OrderDate, order.OrderedFrom);
            if (order.Trainers != null)
            {
                order.Trainers.TimeTable = Models.Graph.RemoveReservation(order.Trainers.TimeTable, order.OrderDate, order.OrderedFrom);
            }
            if (order.ClubCaffeTables != null)
            {
                order.ClubCaffeTables.TimeTable = Models.Graph.RemoveReservation(order.ClubCaffeTables.TimeTable, order.OrderDate, order.OrderedFrom);
            }

            OrderRepository.Update(order);
            OrderRepository.Commit();
        }

        public OrderViewModel GetOrder(int Id)
        {
            Mapper.CreateMap<Orders, OrderViewModel>()
                .ForMember("UserNickname", opt=> opt.MapFrom(c => c.Users.Nickname))
                .ForMember("TrainerFullName", opt => opt.MapFrom(c => c.Trainers.Name +" " + c.Trainers.SecondName))
                .ForMember("ClubName", opt=> opt.MapFrom(c => c.Bowling_club.ClubName))
                .ForMember("Table", opt => opt.MapFrom(c => c.ClubCaffeTables.TableNumber));
            return Mapper.Map<OrderViewModel>(OrderRepository.Find(Id));
        }

        public ICollection<OrderViewModel> GetAllOrders()
        {
            Mapper.CreateMap <Orders, OrderViewModel>()
                                .ForMember("UserNickname", opt => opt.MapFrom(c => c.Users.Nickname))
                .ForMember("TrainerFullName", opt => opt.MapFrom(c => c.Trainers.Name + " " + c.Trainers.SecondName))
                .ForMember("ClubName", opt => opt.MapFrom(c => c.Bowling_club.ClubName))
                .ForMember("Table", opt => opt.MapFrom(c => c.ClubCaffeTables.TableNumber)); ;
            return Mapper.Map<ICollection<OrderViewModel>>(OrderRepository.GetAll());
        }

        public void OrderPaymentPlan(int Id, string cardCode, string cardHolder)
        {
            var order = OrderRepository.Find(Id);
            order.Status = (int)OrderStatus.Payed;
        }

        private string FindFreeLanes(Bowling_club Club, DateTime OrderDate, int OrderedFor, int OrderedFrom, int LanesOrdered)
        {
            {
                var res = new Collection<int>();
                int count = 0;
                List<Models.Graph> timeTables = new List<Models.Graph>();

                foreach (Lanes lane in Club.Lanes)
                {
                    if(!String.IsNullOrEmpty(lane.TimeTable))
                    timeTables.Add(Models.Graph.ToObj(lane.TimeTable));
                    else timeTables.Add(new Models.Graph());
                    if (timeTables.Last().IsFree(OrderDate, OrderedFrom, OrderedFor))
                    {
                        res.Add(lane.LaneNumber);
                        if (++count == LanesOrdered)
                            break;
                    }
                    else timeTables.Remove(timeTables.Last());
                        
                }

                if(count != LanesOrdered)
                {
                    throw (new Exception("Not enough free lanes for " + OrderDate.AddHours(OrderedFrom)));
                }

                for(int i=0; i<LanesOrdered; i++)
                {
                    timeTables[i].Add(OrderDate, OrderedFrom, OrderedFor);
                    Club.Lanes.First(x => x.LaneNumber == res[i]).TimeTable = timeTables[i].ToString();
                }

                string LaneNumbers = String.Empty;

                for (int i = 0; i < res.Count - 1; i++)
                {
                    LaneNumbers += res.ElementAt(i).ToString() + ',';
                }
                LaneNumbers += res.Last().ToString();

                return LaneNumbers;
            }
        }

        private ClubCaffeTables FindFreeTable(Bowling_club Club, DateTime OrderDate, int OrderedFor, int OrderedFrom)
        {
            var Tables = Club.ClubCaffeTables;
            Models.Graph timeTable;

            foreach (var table in Tables)
            {
                timeTable = Models.Graph.ToObj(table.TimeTable);
                if (timeTable.IsFree(OrderDate, OrderedFor, OrderedFrom))
                {
                    timeTable.Add(OrderDate, OrderedFor, OrderedFrom);
                    table.TimeTable = timeTable.ToString();
                    return table;
                }
            }
            throw (new Exception("No tables are free for tihis time"));
        }

        private Trainers FindTrainer(DateTime dt, int start, int hours)
        {
            var Trainers = RepositoryFactory.MakeTrainersRepository(GetDBContext()).GetAll();
            Models.Graph timeTable;
            foreach (Trainers trainer in Trainers)
            {
                timeTable = Models.Graph.ToObj(trainer.TimeTable);
                if (timeTable.IsFree(dt, start, hours))
                {
                    timeTable.Add(dt, start, hours);
                    trainer.TimeTable = timeTable.ToString();
                    return trainer;
                }
            }
            throw (new Exception("No trainers are free for tihis time"));
        }

        private decimal CountDiscount(Users user, Orders order)
        {
            decimal Discount = 0m;

            if (user.Amount >= 5000)
                Discount = 0.2m;
            else if (user.Amount >= 2500)
                Discount = 0.1m;
            else if (user.Amount >= 1000)
                Discount = 0.05m;
            else if (user.Amount > 0 && user.HasCancellations == false)
                Discount = 0.02m;
            else Discount = 0;

            order.Price = order.Price * (1 - Discount);

            return Discount;
        }

        private decimal CountPrice(DateTime OrderTime, int OrderedFor, int LanesOrdered)
        {
            int dayOfWeek = (int)OrderTime.DayOfWeek;

            if (dayOfWeek >= 1 && dayOfWeek <= 4)
            {
                if (OrderTime.Hour >= 18)
                    return 160 * OrderedFor * LanesOrdered;
                else return 120 * OrderedFor * LanesOrdered;
            }
            else
            {
                if (OrderTime.Hour >= 18)
                    return 200 * OrderedFor * LanesOrdered;
                else return 160 * OrderedFor * LanesOrdered;
            }
        }        
    }
}
