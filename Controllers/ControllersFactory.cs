﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Controllers.Controllers_Interaces;

namespace Controllers
{
    public static class ControllersFactory
    {
        public static IUserController Users()
        {
            return new UserController();
        }

        public static IClubController Clubs()
        {
            return new BowlingController();
        }

        public static IOrderController Orders()
        {
            return new OrderController();
        }

        public static ITrainerController Trainers()
        {
            return new TrainerController();
        }
    }
}
