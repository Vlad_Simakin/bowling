﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resources.OrderStatus
{
    public enum OrderStatus:int
    {
        Created = 1,
        Payed = 2,
        Cancelled = 3
    }
}