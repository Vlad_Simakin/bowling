﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controllers;
using System.Collections.ObjectModel;
namespace Bowling_system
{
    class Program
    {
        static void Main(string[] args)
        {
            int clubId;
            int userId;

            using (var Trainers = ControllersFactory.Trainers())
            {
                //     Trainers.AddTrainer("Tr1", "Tr_SName1", "12312313", "tr@bk.ua");
            }
            using (var Users = ControllersFactory.Users())
            {
                Users.CrateUser("User1", "User Name", "User Surname");
                userId = Users.GetAllUsers().Last().Id;
                Users.BuySeasonTicket(userId, 1);
            }
            using (var Clubs = ControllersFactory.Clubs())
            {
                Clubs.CreateClub("New Club123", "Some Street", 10, 2, 2, 5);
                clubId = Clubs.GetAllClubs().Last().Id;
                //Clubs.DeleteClub(clubId);
            }
            using (var Orders = ControllersFactory.Orders())
            {
                var order = Orders.MakeOrder(clubId, userId, new DateTime(2015, 10, 2), 11, 2, 1, true, true);
                var order1 = Orders.MakeOrder(clubId, userId, new DateTime(2015, 10, 2), 11, 1, 1, true, true);
                Console.WriteLine("Order ID: {0}\nTotal cost: {1}\nDiscount: {2}%\nOrdered Date: {3}\n -For {4} hours\nStatus {5}\nLane(s): {6}\n",
                        order.OrderId, order.Price, order.Discount * 100, order.OrderDate.AddHours(order.OrderedFrom), order.OrderedFor, order.Status, order.LaneNumbers);

                Orders.CancelOrder(order);
            }
            Console.Read();
        }
    }
}
