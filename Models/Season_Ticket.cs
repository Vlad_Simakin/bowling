﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Season_Ticket
    {
        [Key]
        public int Id { get; set; }
        public decimal Price { get; set; }
        public int ForMonths { get; set; }
        public DateTime ExpirationDate { get; set; }

        public virtual Users User { get; set; }
    }
}
