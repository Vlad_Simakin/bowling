﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    public class Users
    {
        [Key]
        public int Id { get; set; }
        public String Nickname { get; set; }
        public String UserName { get; set; }
        public String SecondName { get; set; }
        public int HighScore { get; set; }
        public bool HasCancellations { get; set; }
        public decimal Amount { get; set; }
        public string CreditCard { get; set; } //for now

        public Users()
        {
            Orders = new List<Orders>();
            Season_Tickets = new List<Season_Ticket>();
        }

        public virtual ICollection<Orders> Orders { get; set; }
        public virtual ICollection<Season_Ticket> Season_Tickets { get; set; }
    }
}
