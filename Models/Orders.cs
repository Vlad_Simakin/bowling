﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.ObjectModel;

namespace Models
{
    public class Orders
    {
        [Key]
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public int OrderedFrom { get; set; }
        public int OrderedFor { get; set; }
        public int Status { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public string LaneNumbers { get; set; }

        [NotMapped]
        public ICollection<int> lanesNumbers
        {
            get
            {
                string[] Parts = this.LaneNumbers.Split(',');
                var res = new Collection<int>();

                foreach(var part in Parts)
                {
                    res.Add(int.Parse(part));
                }
                return res;
            }
            set
            {
                LaneNumbers = String.Empty;

                for (int i = 0; i < value.Count-1; i++ )
                {
                    LaneNumbers += value.ElementAt(i).ToString() +',';
                }
                LaneNumbers += value.Last().ToString();
            }
        }

        public virtual Bowling_club Bowling_club { get; set; }
        public virtual ClubCaffeTables ClubCaffeTables { get; set; }
        public virtual Trainers Trainers { get; set; }
        public virtual Users Users { get; set; }
    }
}
 
