﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Graph
    {
        public SortedDictionary<DateTime, SortedDictionary<int, int>> TimeTable = new SortedDictionary<DateTime, SortedDictionary<int, int>>();

        public override string ToString()
        {

                StringBuilder builder = new StringBuilder();

                foreach (KeyValuePair<DateTime, SortedDictionary<int, int>> date in TimeTable)
                {
                    builder.Append('|');
                    builder.Append(date.Key);
                    foreach (KeyValuePair<int, int> time in date.Value)
                    {
                        builder.AppendFormat("\n{0}-{1}", time.Key, time.Value);
                    }
                    //builder.Append('|');
                }

                return builder.ToString();
        }

        public static string RemoveReservation(string encoded, DateTime OrderDate, int OrderedFrom)
        {
            var schedule = ToObj(encoded);
            schedule.TimeTable[OrderDate].Remove(OrderedFrom);

            return schedule.ToString();
        }

        public static Graph ToObj(string encoded)
        {
            Graph newTable = new Graph();

            if (encoded == null)
                return newTable;

            string trimmed = encoded.TrimEnd();
            string[] parts = trimmed.Split('|');
            for(int j = 1; j < parts.Length; j += 1)
            {
                var smallParts = parts[j].Split('\n','-');
                var date = Convert.ToDateTime(smallParts[0]);
                for (int i = 1; i < smallParts.Length; i += 2)
                {
                    newTable.Add(date,int.Parse(smallParts[i]), int.Parse(smallParts[i + 1]));
                }
            }

            return newTable;
        }

        public bool IsFree(DateTime dt, int start, int hours)
        {
            try
            {
                if (TimeTable.ContainsKey(dt))
                {
                    var iter = TimeTable[dt].GetEnumerator();
                    while (iter.MoveNext())
                    {

                        if (start + hours <= iter.Current.Key)
                        {
                            return true;
                        }
                        else if (iter.Current.Value <= start)
                        {
                            if (!iter.MoveNext())
                            {
                                return true;
                            }
                            else if (iter.Current.Key >= start + hours)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else return false;
                    }
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void ShowTimeTable()
        {
            foreach (KeyValuePair<DateTime, SortedDictionary<int, int>> date in TimeTable)
            {
                Console.WriteLine(date.Key);
                foreach (KeyValuePair<int, int> time in date.Value)
                    Console.WriteLine("{0} {1}", time.Key, time.Value);
            }
        }

        public void ShowTimeTable(DateTime dt)
        {
            foreach (KeyValuePair<int, int> time in TimeTable[dt])
                Console.WriteLine("{0} {1}", time.Key, time.Value);
        }

        public void Add(DateTime dt, int start, int hours)
        {
            if (TimeTable.ContainsKey(dt))
                TimeTable[dt].Add(start, start + hours);
            else
            {
                TimeTable.Add(dt, new SortedDictionary<int, int>());
                TimeTable[dt].Add(start, start + hours);
            }
        }
    }
}
