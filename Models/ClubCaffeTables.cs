﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ClubCaffeTables
    {
        [Key]
        public int Id { get; set; }
        public int TableNumber { get; set; }
        public string TimeTable
        {
            get
            {
                if (timeTable != null)
                {
                    stable = timeTable.ToString();
                }

                return stable;
            }
            set
            {
                stable = value;
            }
        }

        public Graph timeTable
        {
            get
            {
                if (table == null)
                {

                    if (!String.IsNullOrEmpty(stable))
                    {
                        table = Graph.ToObj(stable);
                    }
                    else
                    {
                        table = new Graph();
                    }
                }

                return table;
            }
        }

        private Graph table;
        private string stable;

        public virtual Bowling_club Club { get; set; }
    }
}
