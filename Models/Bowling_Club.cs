﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
        public class Bowling_club
        {
            public Bowling_club()
        {

            this.ClubCaffeTables = new List<ClubCaffeTables>();
            this.Lanes = new List<Lanes>();
            this.Orders = new List<Orders>();
        }
    
        public int Id { get; set; }
        public string ClubName { get; set; }
        public string Address { get; set; }
        public int Opening { get; set; }
        public int Closing { get; set; }
    
        public virtual ICollection<ClubCaffeTables> ClubCaffeTables { get; set; }
        public virtual ICollection<Lanes> Lanes { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
}
}
