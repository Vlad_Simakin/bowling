﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Trainers
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Schedule
        {
            get
            {
                if (table != null)
                {
                    stable = table.ToString();
                }

                return stable;
            }
            set
            {
                stable = value;
            }
        }

        public Graph scheduleClass
        {
            get
            {
                if (table == null)
                {

                    if (!String.IsNullOrEmpty(stable))
                    {
                        table = Graph.ToObj(stable);
                    }
                    else
                    {
                        table = new Graph();
                    }
                }

                return table;
            }
        }

        private Graph table;
        private string stable;
    }
}
