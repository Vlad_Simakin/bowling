﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;
using Controllers.ViewModels;

namespace BowlingConsoleClient.Commands.Users
{
    class ShowUserOrderHistory:UserCommand
    {
        public ShowUserOrderHistory(TextWriter output, EntityReference currentUser)
            :base("user.history",output,currentUser)
        { }

        public override void Execute(CommandSwitchValues values)
        {
            ValidateLogged();

            using (var userController = Controllers.ControllersFactory.Users())
            {
                var orders = userController.GetUserHistory(CurrentUser.ObjectId);
                foreach (OrderViewModel order in orders)
                {
                    Output.WriteLine(order.ToString());
                }
            }
        }
    }
}
