﻿using System.IO;
using BowlingConsoleClient.Commands.Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingConsoleClient.Commands.Users
{
    abstract class UserCommand: Command
    {
        protected UserCommand(string name, TextWriter output, EntityReference currentUser) :
            base(name, output)
        {
            this.CurrentUser = currentUser;
        }

        protected void ValidateLogged()
        {
            if (!CurrentUser.IsInitialized())
                throw new Exception("No customer logged in");
        }

        protected EntityReference CurrentUser { get; private set; }
    }
}
