﻿using BowlingConsoleClient.Commands.Basic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingConsoleClient.Commands.Users
{
    class UsersCountCommand:Command
    {
        public UsersCountCommand( TextWriter output )
            : base( "user.showall", output )
        {
        }


        public override void Execute ( CommandSwitchValues values )
        {
            using ( var userController = Controllers.ControllersFactory.Users() )
            {
                var users = userController.GetAllUsers();
                ReportValues( users );
            }
        }
    }
}
