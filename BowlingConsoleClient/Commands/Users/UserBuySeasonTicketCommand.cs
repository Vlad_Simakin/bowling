﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;

namespace BowlingConsoleClient.Commands.Users
{
    class UserBuySeasonTicketCommand:UserCommand
    {
        public UserBuySeasonTicketCommand(TextWriter output, EntityReference currentUser)
            : base("user.ticket",output, currentUser)
        {
            AddSwitch(new CommandSwitch("-months",CommandSwitch.ValueMode.ExpectSingle, false));
        }

        public override void Execute(CommandSwitchValues values)
        {
            int moths = values.GetSwitchAsInt("-months");

            using (var userController = Controllers.ControllersFactory.Users())
            {
                userController.BuySeasonTicket(CurrentUser.ObjectId,moths);
                Output.WriteLine("Season ticket bought!");
            }
        }
    }
}
