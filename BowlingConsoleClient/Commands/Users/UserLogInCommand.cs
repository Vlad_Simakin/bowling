﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;

namespace BowlingConsoleClient.Commands.Users
{
    class UserLogInCommand: UserCommand
    {
        public UserLogInCommand(TextWriter output, EntityReference currentUser)
            :base("user.logIn", output, currentUser)
        {
            AddSwitch(new CommandSwitch("-nickname", CommandSwitch.ValueMode.ExpectSingle, false));            
        }

        public override void Execute(CommandSwitchValues values)
        {
            string login = values.GetSwitch("-nickname");

            using (var userController = Controllers.ControllersFactory.Users())
            {
                var user = userController.GetUserByNickname(login);
                CurrentUser.Initialize(user.Id);
                Output.WriteLine("Welcome" + user.Nickname);
            }
        }
    }
}
