﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;

namespace BowlingConsoleClient.Commands.Users
{
    class UserInfoCommand:UserCommand
    {
        public UserInfoCommand(TextWriter output, EntityReference currentUser)
            :base("user.info",output,currentUser)
        { }

        public override void Execute(CommandSwitchValues values)
        {
            using (var userController = Controllers.ControllersFactory.Users())
            {
                Output.WriteLine( userController.GetUser(CurrentUser.ObjectId).ToString());
            }
        }
    }
}
