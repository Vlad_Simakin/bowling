﻿using BowlingConsoleClient.Commands.Basic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingConsoleClient.Commands.Users
{
    class RegisterUserCommand:UserCommand
    {
        public RegisterUserCommand(TextWriter output, EntityReference currentUser)
            : base("user.register", output, currentUser)
        {
            AddSwitch(new CommandSwitch("-nickname", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-name", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-surname", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-email", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-addr", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-phone", CommandSwitch.ValueMode.ExpectSingle, false));
        }

        public override void Execute(CommandSwitchValues values)
        {
            string nickname = values.GetSwitch("-nickname");
            string name = values.GetSwitch("-name");
            string surname = values.GetSwitch("-surname");
            string email = values.GetSwitch("-email");
            string addr = values.GetSwitch("-addr");
            string phone = values.GetSwitch("-phone");

            using (var userController = Controllers.ControllersFactory.Users())
            {
                int userId = userController.CreateUser(nickname, name, surname, email, phone);
                Output.WriteLine("Welcome " + nickname + "!");
                CurrentUser.Initialize(userId);
            }
        }
    }
}
