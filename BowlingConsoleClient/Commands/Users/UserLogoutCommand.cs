﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;

namespace BowlingConsoleClient.Commands.Users
{
    class UserLogoutCommand: UserCommand
    {
        public UserLogoutCommand(TextWriter output, EntityReference currentUser)
            :base("user.logout",output, currentUser)
        { }

        public override void Execute(CommandSwitchValues values)
        {
            if (CurrentUser.IsInitialized())
            {
                Output.WriteLine("Goodbye! ");
                CurrentUser.Drop();
            }

            else
                throw new Exception("No customer logged in");
        }
    }
}
