﻿using System.IO;
using BowlingConsoleClient.Commands.Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controllers.ViewModels;

namespace BowlingConsoleClient.Commands.Orders
{
    class OrderListCommand: Command
    {
        public OrderListCommand(TextWriter output)
            : base("orders.list", output)
        {
            
        }

        public override void Execute(CommandSwitchValues values)
        {
            using (var orderController = Controllers.ControllersFactory.Orders())
            {
                var orders = orderController.GetAllOrders();
                foreach (OrderViewModel order in orders)
                {
                    Output.WriteLine(order.ToString());
                }
            }
        }
    }
}
