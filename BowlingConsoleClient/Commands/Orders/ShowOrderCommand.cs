﻿using System.IO;
using BowlingConsoleClient.Commands.Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingConsoleClient.Commands.Orders
{
    class ShowOrderCommand: Command
    {
        public ShowOrderCommand(TextWriter output)
            : base("orders.show", output)
        {
            AddSwitch(new CommandSwitch("-id", CommandSwitch.ValueMode.ExpectSingle, false));
        }

        public override void Execute(CommandSwitchValues values)
        {
            var id = values.GetSwitchAsInt("-id");

            using (var orderController = Controllers.ControllersFactory.Orders())
            {
                Output.WriteLine(orderController.GetOrder(id).ToString());
            }
        }
    }
}
