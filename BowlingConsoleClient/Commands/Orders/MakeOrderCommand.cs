﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;

namespace BowlingConsoleClient.Commands.Orders
{
    class MakeOrderCommand: Command
    {
        public MakeOrderCommand(TextWriter output)
            : base("orders.make", output)
        {
            AddSwitch(new CommandSwitch("-clubid", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-userid", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-orderDate",CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-orderHour", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-orderFor", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-lanes", CommandSwitch.ValueMode.ExpectSingle, true));
            AddSwitch(new CommandSwitch("-table", CommandSwitch.ValueMode.ExpectSingle, true));
            AddSwitch(new CommandSwitch("-trainer", CommandSwitch.ValueMode.ExpectSingle, true));
        }

        public override void Execute(CommandSwitchValues values)
        {
            bool trainer = values.GetSwitchAsBool("-trainer");
            bool table = values.GetSwitchAsBool("-table");
            int lanes = values.GetSwitchAsInt("-lanes");
            int orderedFor = values.GetSwitchAsInt("-orderFor");
            int orderHour = values.GetSwitchAsInt("-orderHour");
            var orderDate = values.GetSwichAsDateTime("-orderDate");
            int userId = values.GetSwitchAsInt("-userid");
            int clubId = values.GetSwitchAsInt("clubid");

            using (var orderController = Controllers.ControllersFactory.Orders())
            {
                orderController.MakeOrder(clubId, userId, orderDate, orderHour, orderedFor, lanes, table, trainer);
            }
        }
    }
}
