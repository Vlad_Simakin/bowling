﻿using System.IO;
using BowlingConsoleClient.Commands.Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingConsoleClient.Commands.Orders
{
    class CancelOrderCommand: Command
    {
        public CancelOrderCommand(TextWriter output) :
            base("orders.cancel", output)
        {
            AddSwitch(new CommandSwitch("-id", CommandSwitch.ValueMode.ExpectSingle, false));
        }

        public override void Execute(CommandSwitchValues values)
        {
            int id = values.GetSwitchAsInt("-id");

            using (var orderController = Controllers.ControllersFactory.Orders())
            {
                orderController.CancelOrder(id);
                Output.WriteLine("Order "+id+" is cancelled");
            }
        }
    }
}
