﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;

namespace BowlingConsoleClient.Commands.Payment
{
    class MakeCardPaymentCommand: Command
    {
        public MakeCardPaymentCommand(TextWriter output, EntityReference currentOrder)
            : base("payment.pay", output)
        {
            AddSwitch(new CommandSwitch("-code", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-holder", CommandSwitch.ValueMode.ExpectSingle, false));

            this.currentOrder = currentOrder;
        }

        public override void Execute(CommandSwitchValues values)
        {
            if (!currentOrder.IsInitialized())
                throw new Exception("No open order to pay for...");

            string cardCode = values.GetSwitch("-code");
            string cardHolder = values.GetSwitch("-holder");

            using (var orderController = Controllers.ControllersFactory.Orders())
            {
                orderController.OrderPaymentPlan(currentOrder.ObjectId, cardCode, cardHolder);
                currentOrder.Drop();
            }
        }

        private EntityReference currentOrder;
    }
}
