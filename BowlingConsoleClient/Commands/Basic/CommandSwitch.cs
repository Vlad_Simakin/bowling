﻿using System;

namespace BowlingConsoleClient.Commands.Basic
{
    class CommandSwitch
    {
        public string Name { get; private set; }


        public bool Optional { get; private set; }


        public enum ValueMode
        {
            Unexpected,
            ExpectSingle,
            ExpectMultiple
        };


        public ValueMode Mode { get; private set; }


        public CommandSwitch ( string name, ValueMode mode, bool optional )
        {
            if ( name.Length == 0 )
                throw new Exception( "CommandSwitch: empty name not allowed" );

            this.Name = name;
            this.Mode = mode;
            this.Optional     = optional;
        }
    }
}
