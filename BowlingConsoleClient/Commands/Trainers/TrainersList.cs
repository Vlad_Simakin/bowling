﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;
using Controllers.ViewModels;

namespace BowlingConsoleClient.Commands.Trainers
{
    class TrainersList: Command
    {
        public TrainersList(TextWriter output)
            :base("trainers.list", output)
        { }

        public override void Execute(CommandSwitchValues values)
        {
            using (var trainerController = Controllers.ControllersFactory.Trainers())
            {
                var trainers = trainerController.ListOfTrainers();
                foreach (TrainerViewModel trainer in trainers)
                {
                    Output.WriteLine(trainer.ToString());
                }
            }
        }
    }
}
