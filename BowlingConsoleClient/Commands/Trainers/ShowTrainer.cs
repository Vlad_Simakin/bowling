﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;

namespace BowlingConsoleClient.Commands.Trainers
{
    class ShowTrainer: Command
    {
        public ShowTrainer(TextWriter output)
            : base("trainers.show", output)
        {
            AddSwitch(new CommandSwitch("-id", CommandSwitch.ValueMode.ExpectSingle, false));
        }

        public override void Execute(CommandSwitchValues values)
        {
            var id = values.GetSwitchAsInt("-id");

            using (var trainersController = Controllers.ControllersFactory.Trainers())
            {
                Output.WriteLine(trainersController.FindTrainerById(id).ToString());
            }
        }
    }
}
