﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;

namespace BowlingConsoleClient.Commands.Trainers
{
    class HireTrainer: Command
    {
        public HireTrainer(TextWriter output)
            : base("trainers.hire", output)
        {
            AddSwitch(new CommandSwitch("-name",CommandSwitch.ValueMode.ExpectSingle,false));
            AddSwitch(new CommandSwitch("-surname", CommandSwitch.ValueMode.ExpectSingle, false)); 
            AddSwitch(new CommandSwitch("-email", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-phone", CommandSwitch.ValueMode.ExpectSingle, false));
        }
        public override void Execute(CommandSwitchValues values)
        {
            string name = values.GetSwitch("-name");
            string surname = values.GetSwitch("-surname");
            string email = values.GetSwitch("-email");
            string phone = values.GetSwitch("-phone");

            using (var trainersController = Controllers.ControllersFactory.Trainers())
            {
                trainersController.AddTrainer(name, surname, phone, email);
                Output.WriteLine("Trainer " + name + " " + surname + " hired!");
            }
        }
    }
}
