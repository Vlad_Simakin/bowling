﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;
using Controllers.ViewModels;

namespace BowlingConsoleClient.Commands.Trainers
{
    class ShowTrainerOrders:Command
    {
        public ShowTrainerOrders(TextWriter output)
            : base("trainers.orders", output)
        {
            AddSwitch(new CommandSwitch("-id", CommandSwitch.ValueMode.ExpectSingle, false));
        }

        public override void Execute(CommandSwitchValues values)
        {
            var id = values.GetSwitchAsInt("-id");

            using (var trainerController = Controllers.ControllersFactory.Trainers())
            {
                var orders = trainerController.TrainerOrders(id);
                foreach (TrainerOrderViewModel order in orders)
                {
                    Output.WriteLine(order.ToString());
                }
            }
        }
    }
}
