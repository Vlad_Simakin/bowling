﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;

namespace BowlingConsoleClient.Commands.Bowling_Clubs
{
    class ClubInfoCommand: Command
    {
        public ClubInfoCommand(TextWriter output)
            : base("clubs.info", output)
        {
            AddSwitch(new CommandSwitch("-id", CommandSwitch.ValueMode.ExpectSingle, false));
        }

        public override void Execute(CommandSwitchValues values)
        {
            using (var clubController = Controllers.ControllersFactory.Clubs())
            {
                Output.WriteLine(clubController.GetClub(values.GetSwitchAsInt("-id")));
            }
        }
    }
}
