﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controllers.ViewModels;
using BowlingConsoleClient.Commands.Basic;
using System.IO;

namespace BowlingConsoleClient.Commands.Bowling_Clubs
{
    class ClubLanesShowCommand : Command
    {
        public ClubLanesShowCommand(TextWriter output)
            :base("clubs.lanes", output)
        {
            AddSwitch(new CommandSwitch("-id", CommandSwitch.ValueMode.ExpectSingle, false));
        }

        public override void Execute(CommandSwitchValues values)
        {
            var id = values.GetSwitchAsInt("-id");

            using (var clubsController = Controllers.ControllersFactory.Clubs())
            {
                var lanes = clubsController.ClubLanes(id);
                foreach (LaneViewModel lane in lanes)
                {
                    Output.WriteLine(lane.ToString());
                }
            }
        }
    }
}
