﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;
using Controllers;

namespace BowlingConsoleClient.Commands.Bowling_Clubs
{
    class CreateClubCommand: Command
    {
        public CreateClubCommand(TextWriter output)
            :base("club.create", output)
        {
            AddSwitch(new CommandSwitch("-name", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-addr", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-opening", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-closing", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-lanes", CommandSwitch.ValueMode.ExpectSingle, false));
            AddSwitch(new CommandSwitch("-tables", CommandSwitch.ValueMode.ExpectSingle, false));
        }

        public override void Execute(CommandSwitchValues values)
        {
            string name = values.GetSwitch("-name");
            string address = values.GetSwitch("-addr");
            int opening = values.GetSwitchAsInt("-opening");
            int closing = values.GetSwitchAsInt("-closing");
            int lanes = values.GetSwitchAsInt("-lanes");
            int tables = values.GetSwitchAsInt("-tables");

            using (var clubController = ControllersFactory.Clubs())
            {
                clubController.CreateClub(name, address, opening, closing, lanes, tables);
                Output.WriteLine("Club {0} created successfully", name);
            }
        }
    }
}
