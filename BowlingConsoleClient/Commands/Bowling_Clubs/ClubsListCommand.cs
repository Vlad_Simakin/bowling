﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;
using Controllers.ViewModels;

namespace BowlingConsoleClient.Commands.Bowling_Clubs
{
    class ClubsListCommand: Command
    {
        public ClubsListCommand(TextWriter output)
            : base("clubs.list", output) { }

        public override void Execute(CommandSwitchValues values)
        {
            using (var clubControoler = Controllers.ControllersFactory.Clubs())
            {
                var clubs = clubControoler.GetAllClubs();
                foreach (BowlingViewModel club in clubs)
                {
                    Output.WriteLine(club.ToString());
                }
            }
        }
    }
}
