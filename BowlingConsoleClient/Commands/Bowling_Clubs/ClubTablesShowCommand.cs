﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Basic;
using System.IO;
using Controllers.ViewModels;

namespace BowlingConsoleClient.Commands.Bowling_Clubs
{
    class ClubTablesShowCommand:Command
    {
        public ClubTablesShowCommand(TextWriter output)
            : base("clubs.tables", output)
        {
            AddSwitch(new CommandSwitch("-id", CommandSwitch.ValueMode.ExpectSingle, false));
        }

        public override void Execute(CommandSwitchValues values)
        {
            var id = values.GetSwitchAsInt("-id");

            using (var clubsController = Controllers.ControllersFactory.Clubs())
            {
                var tables = clubsController.ClubTables(id);
                foreach (TableViewModel table in tables)
                {
                    Output.WriteLine(table.ToString());
                }

            }
        }
    }
}
