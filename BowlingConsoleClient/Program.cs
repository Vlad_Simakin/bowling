﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingConsoleClient.Commands.Bowling_Clubs;
using BowlingConsoleClient.Commands.Orders;
using BowlingConsoleClient.Commands.Payment;
using BowlingConsoleClient.Commands.Trainers;
using BowlingConsoleClient.Commands.Users;
using Controllers.ViewModels;
using System.IO;
using BowlingConsoleClient.Commands.Basic;

namespace BowlingConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            RunCommandProcessingLoop(System.Console.In, System.Console.Out);
        }

        private static void RunCommandProcessingLoop(TextReader input, TextWriter output)
        {
            CommandHandler commandHandler = new CommandHandler();

            EntityReference currentCart = new EntityReference();
            EntityReference currentOrder = new EntityReference();
            EntityReference currentCustomer = new EntityReference();

            InitCommands(commandHandler, output, currentCart, currentOrder, currentCustomer);

            while (true)
            {
                output.Write("> ");
                string command = input.ReadLine();

                try
                {
                    commandHandler.ProcessCommandLine(command);
                }
                catch (Exception e)
                {
                    output.WriteLine(
                        string.Format(
                            "Error: {0}\n{1}\n",
                            e.Message,
                            (e.InnerException != null) ? e.InnerException.Message : ""
                        )
                    );
                }
            }
        }


        private static void InitCommands(
            CommandHandler handler,
            TextWriter output,
            EntityReference currentCart,
            EntityReference currentOrder,
            EntityReference currentCustomer
        )
        {
            handler.RegisterCommand(new HelpCommand(handler, output));
            handler.RegisterCommand(new QuitCommand(output));

            handler.RegisterCommand(new ClubInfoCommand(output));
            handler.RegisterCommand(new ClubLanesShowCommand(output));
            handler.RegisterCommand(new ClubsListCommand(output));
            handler.RegisterCommand(new ClubTablesShowCommand(output));
            handler.RegisterCommand(new CreateClubCommand(output));

            handler.RegisterCommand(new CancelOrderCommand(output));
            handler.RegisterCommand(new MakeOrderCommand(output));
            handler.RegisterCommand(new OrderListCommand(output));
            handler.RegisterCommand(new ShowOrderCommand(output));

            handler.RegisterCommand(new HireTrainer(output));
            handler.RegisterCommand(new ShowTrainer(output));
            handler.RegisterCommand(new ShowTrainerOrders(output));
            handler.RegisterCommand(new TrainersList(output));

            handler.RegisterCommand(new RegisterUserCommand(output, currentCustomer));
            handler.RegisterCommand(new ShowUserOrderHistory(output, currentCustomer));
            handler.RegisterCommand(new UserBuySeasonTicketCommand(output, currentCustomer));
            handler.RegisterCommand(new UserInfoCommand(output, currentCustomer));
            handler.RegisterCommand(new UserLogInCommand(output, currentCustomer));
            handler.RegisterCommand(new UserLogoutCommand(output, currentCustomer));
            handler.RegisterCommand(new UsersCountCommand(output));
        }
    }
}
