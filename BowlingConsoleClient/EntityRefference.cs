﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingConsoleClient
{
    class EntityReference
    {
        public EntityReference()
        {
            this.ObjectId = -1;
        }


        public int ObjectId { get; private set; }


        public bool IsInitialized()
        {
            return ObjectId != -1;
        }


        public void Initialize(int objectId)
        {
            this.ObjectId = objectId;
        }


        public void Drop()
        {
            this.ObjectId = -1;
        }
    }
}
