﻿using System;

namespace Infrastructure
{
    public interface ISmsAgent
    {
        void sendSMS ( string phoneNumber, string body );
    }
}
